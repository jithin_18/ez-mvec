from django.db import models

# Create your models here.
class Users(models.Model):
    active_status = models.BooleanField(default=True)
    firstname = models.CharField(max_length=200)
    lastname = models.CharField(max_length=200,blank=True)
    contact_number = models.CharField(max_length=100,blank=True)
    email = models.EmailField(max_length=200,blank=True)
    username = models.CharField(max_length=200,null=True,blank=True)
    place = models.CharField(max_length=200,null=True,blank=True)
    latitude = models.CharField(max_length=200,null=True,blank=True)
    longitude = models.CharField(max_length=200,null=True,blank=True)
    password = models.CharField(max_length=200,null=True,blank=True)

