from .models import Users
from rest_framework import serializers

class UsersSerializer(serializers.ModelSerializer):
    class Meta:
        model=Users
        fields = ['id','firstname','lastname','contact_number','email','username','password','place','latitude','longitude']
class UsersGetSerializer(serializers.ModelSerializer):
    class Meta:
        model=Users
        fields='__all__'        