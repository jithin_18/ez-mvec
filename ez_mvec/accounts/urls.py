from django.contrib import admin
from django.urls import path, include

from . import views

urlpatterns = [
    path('create/', views.userCreateAPI, name='userCreateAPI'),
    path('list/', views.UserGetAllAPI, name='UserGetAllAPI'),
    path('login/', views.userLogin, name='userLogin'),
]