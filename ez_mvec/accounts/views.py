from django.shortcuts import render
from .models import Users
import os
import datetime
from django.shortcuts import render, HttpResponse

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework import status
from rest_framework.permissions import BasePermission, IsAuthenticated, SAFE_METHODS
from django.db.models import Q
from rest_framework import status
from django.contrib.auth.models import User

from .serializer import UsersSerializer,UsersGetSerializer



# Create your views here.
@api_view(['POST'])
def userCreateAPI(request):
    if request.method == 'POST':
        try:
        #     print(request.data)
            email = request.POST['email']
            username = request.POST['username']
            password = request.POST['password']  
            serializer = UsersSerializer(data=request.data)
            if serializer.is_valid():
                serializer.save()
            else:    
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST) 
               
            try: 
                if(User.objects.filter(username=username).exists()):
                    return Response({'Error': 'Username is already exist','app_data': 'Invalid credentials'}, status=status.HTTP_400_BAD_REQUEST)             
                user = User.objects.create_user(username=username,email=email,password=password)
                user.save()
                user_temp = Users.objects.get(id=serializer.data['id'])
                user_temp.user=user
                user_temp.save()
                return Response({'Success': 'User created','app_data': 'User Created '}, status.HTTP_201_CREATED)
            except:
                user= Users.objects.get(id=serializer.data['id'])
            
                user.delete()
                user.save() 
                return Response({'Error': 'Error Creating User',
                'app_data': 'User Registration failed'}, status=status.HTTP_400_BAD_REQUEST)  
        except:
            return Response({'Error': 'Error Creating User',
                'app_data': 'User Registration failed'}, status=status.HTTP_400_BAD_REQUEST) 
    
@api_view(['GET'])
@permission_classes([IsAuthenticated])
def UserGetAllAPI(request):
    if request.method == 'GET':
        try:  
            user = Users.objects.filter(active_status=True)          
            serializer = UsersGetSerializer(user,many=True)                   
            return Response(serializer.data)
        except:
            return Response({'Error': 'No such employee found', 'app_data': 'No such employee found '}, status=status.HTTP_400_BAD_REQUEST)  


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def userLogin(request):
    if request.method =='POST':
        print("hjgfdhj")
        username = request.POST['username']
        password = request.POST['password']      
        val = Users.objects.filterQ((username==username) | Q(password==password)).exists()
    
@api_view(['PATH','DELETE'])
@permission_classes([IsAuthenticated])
def clientEditAPI(request, client_id):
    if request.method == 'PATCH':
        try:  
            client = Users.objects.get(client_id=client_id)
            serializer = ClientGetSerializer(client, data=request.data, partial=True)   
            if serializer.is_valid():
                serializer.save()                
            return Response(serializer.data)            
        except:
            return Response({'Error': 'No such Client found', 'app_data': 'No such Client found '}, status=status.HTTP_400_BAD_REQUEST)    
