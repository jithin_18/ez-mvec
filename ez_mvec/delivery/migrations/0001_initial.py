# Generated by Django 3.1.1 on 2020-09-17 13:52

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Address',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('home_number_name', models.CharField(max_length=200)),
                ('Address_line1', models.CharField(blank=True, max_length=200)),
                ('Address_line2', models.CharField(blank=True, max_length=200)),
                ('location', models.CharField(blank=True, max_length=200)),
                ('state', models.CharField(blank=True, max_length=200)),
                ('district', models.CharField(blank=True, max_length=200)),
                ('country', models.CharField(blank=True, max_length=200)),
                ('landmark', models.CharField(blank=True, max_length=200, null=True)),
                ('contact1', models.CharField(blank=True, max_length=100, null=True)),
                ('contact2', models.CharField(blank=True, max_length=100, null=True)),
            ],
        ),
    ]
