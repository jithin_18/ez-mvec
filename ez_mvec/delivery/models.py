from django.db import models
from accounts.models import Users

# Create your models here.
class Address(models.Model):
    active_status=models.BooleanField(default=True)
    user = models.ForeignKey(Users,on_delete=models.CASCADE)
    home_number_name = models.CharField(max_length=200)
    address_line1 = models.CharField(max_length=200,blank=True)
    address_line2 = models.CharField(max_length=200,blank=True)
    location = models.CharField(max_length=200,blank=True)
    state = models.CharField(max_length=200,blank=True)
    district = models.CharField(max_length=200,blank=True)
    country = models.CharField(max_length=200, blank=True)
    landmark = models.CharField(max_length=200,blank=True,null=True)
    contact1 = models.CharField(max_length=100, blank=True,null=True)
    contact2 =models.CharField(max_length=100,blank=True,null=True)    