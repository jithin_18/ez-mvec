from .models import Address
from rest_framework import serializers
class AddressSerializer(serializers.ModelSerializer):
    class Meta:
        model=Address
        fields = ['id','user','home_number_name','address_line1','address_line2','location','state','district','country','contact1','contact2']
class AddressGetSerializer(serializers.ModelSerializer):
    model =Address
    fields='__all__'