import os
import datetime
from django.shortcuts import render, HttpResponse
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework import status
from rest_framework.permissions import BasePermission, IsAuthenticated, SAFE_METHODS
from django.db.models import Q
from rest_framework import status
from django.contrib.auth.models import User
from .models import Address
from .serializer import AddressSerializer,AddressGetSerializer

@api_view(['POST','GET','PATCH','DELETE'])
@permission_classes([IsAuthenticated])
def address(request,address_id=0):
    if request.method=='POST':
        try:

            serializer = AddressSerializer(data=request.data)
            if (serializer.is_valid()):
                serializer.save()
                return Response({'Success':'address created','app_data':'address created'})
        except:
            return Response({'Error':'Error while creating address','app_data':'address not registered'},status=status.HTTP_400_BAD_REQUEST)
    if request.method=='GET':
        print('fgjhfjg  ')
        addr = Address.objects.filter(active_status=True)
        serializer = AddressGetSerializer(addr,many=True)
        print(serializer)
        return Response(serializer.data)    
  


