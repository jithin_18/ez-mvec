from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings
from rest_framework_simplejwt import views as jwt_views
from django.contrib import admin
from django.urls import path

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/token/', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
    path('api/mvec/user/', include('accounts.urls'), name='accounts-api-mvec-parent'),
    path('api/mvec/order/', include('orders.urls'), name='order-api-mvec-parent'),
    path('api/mvec/delivery/', include('delivery.urls'), name='delivery-api-mvec-parent'),
    path('api/mvec/product/', include('product.urls'), name='product-api-mvec-parent'),
    path('api/mvec/vendor/', include('vendor.urls'), name='vendor-api-mvec-parent'),
    path('api/mvec/payment/', include('payment.urls'), name='payment-api-mvec-parent'),
    path('api/mvec/offer/', include('offers.urls'), name='offers-api-mvec-parent'),
]
