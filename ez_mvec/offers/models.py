from django.db import models
from datetime import datetime


# Create your models here.

class DealoftheDay(models.Model):
    active_status=models.BooleanField(default=True)
    name = models.CharField(max_length=200)
    offer = models.IntegerField()
    price =models.CharField(max_length=100)
    image = models.ImageField(upload_to='products/', null=True, blank=True)
    date = models.DateField(default=datetime.now())
    quantity =models.CharField(max_length=200, blank=True)
    