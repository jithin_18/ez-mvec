from rest_framework import serializers
from .models import DealoftheDay

class DealoftheDaySerializer(serializers.ModelSerializer):
    class Meta:
        model = DealoftheDay
        fields =['id','name','price','offer','image']

class DealoftheDayGetSerializer(serializers.ModelSerializer):
    class Meta:
        model = DealoftheDay
        fields = '__all__'