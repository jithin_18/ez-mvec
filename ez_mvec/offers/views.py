from django.shortcuts import render
import os
import datetime
from django.shortcuts import render, HttpResponse

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework import status
from rest_framework.permissions import BasePermission, IsAuthenticated, SAFE_METHODS
from django.db.models import Q
from rest_framework import status
from .models import DealoftheDay
from .serializer import DealoftheDayGetSerializer,DealoftheDaySerializer
# Create your views here.
@api_view(['POST','GET','PATH','DELETE'])
@permission_classes([IsAuthenticated])
def dealofTheDay(request,deal_id=0):
    if request.method=='POST':
        try:

            serializer = DealoftheDaySerializer(data=request.data)
            if serializer.is_valid():
                serializer.save()
                return  Response({'Success':'deal created','app_data':'deal created'})    
        except:
            return Response ({'Error':"error in creating the deal",'app_data':'error while creating deal for today'})      

    if request.method=='GET':
        try:
            print(request.user)
            deal = DealoftheDay.objects.filter(active_status=True)
            serializer = DealoftheDayGetSerializer(deal,many=True)
            return Response (serializer.data)   
        except:
            return Response ({'Error':"error in creating the deal",'app_data':'error while creating deal for today'})          