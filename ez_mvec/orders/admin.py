from django.contrib import admin
from .models import Orders,OrderedProduct,OrderedAddress
# Register your models here.
admin.site.register(OrderedAddress)
admin.site.register(OrderedProduct)
admin.site.register(Orders)