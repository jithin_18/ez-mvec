from django.db import models

from product.models import Product


# Create your models here.
class OrderedProduct(models.Model):
    active_status = models.BooleanField(default=True)
    product = models.ForeignKey(Product,on_delete=models.CASCADE)
    price =models.CharField(max_length=100)
    sale_price =models.CharField(max_length=100)
    ordered_price =models.CharField(max_length=100)
    discount =models.CharField(max_length=100,blank=True,null=True)
    used_coupon =models.CharField(max_length=100,blank=True,null=True)
class OrderedAddress(models.Model):
    active_status = models.BooleanField(default=True)
    home_number_name = models.CharField(max_length=200)
    address_line1 = models.CharField(max_length=200,blank=True)
    address_line2 = models.CharField(max_length=200,blank=True)
    location = models.CharField(max_length=200,blank=True)
    state = models.CharField(max_length=200,blank=True)
    district = models.CharField(max_length=200,blank=True)
    country = models.CharField(max_length=200, blank=True)
    landmark = models.CharField(max_length=200,blank=True,null=True)
    contact1 = models.CharField(max_length=100, blank=True,null=True)
    contact2 =models.CharField(max_length=100,blank=True,null=True)      
class Orders(models.Model):
    active_status = models.BooleanField(default=True)
    product = models.ManyToManyField(OrderedProduct)
    billing_address = models.ManyToManyField(OrderedAddress,related_name='billingAddress')
    shopping_address = models.ManyToManyField(OrderedAddress,related_name='OrderedAddress')
    total = models.CharField(max_length=100)
    total_discount = models.CharField(max_length=100)
    delivery_code = models.CharField(max_length=100)

      