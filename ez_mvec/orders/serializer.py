from rest_framework import serializers
from .models import OrderedProduct,OrderedAddress,Orders

class OrderedProductSerializer(serializers.ModelSerializer):
    class Meta:
        model=OrderedProduct
        fields = ['id','product','price','sale_price','ordered_price','discount','used_coupon']
class OrderedProductGetSerializer(serializers.ModelSerializer):
     class Meta:
         model =OrderedProduct
         fields = '__all__'     
class OrderedAddressSerializer(serializers.ModelSerializer):
    class Meta:
        model=OrderedAddress
        fields=['id','home_number_name','address_line1','address_line2',
        'location','landmark','state','district','country','contact1','contact2']    
class OrderedAddressGetSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderedAddress
        fields = '__all__'    
class OrdersSerializer(serializers.ModelSerializer):
    class Meta:
        model = Orders
        fields = ['id','product','billing_address','shopping_address','total','total_discount','delivery_code']

class OrdersGetSerializer(serializers.ModelSerializer):
    class Meta:
        model =Orders
        fields = '__all__'