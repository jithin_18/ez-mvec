from django.urls import path
from . import views

urlpatterns  =[
    path('product/',views.orderProduct,name='orderProduct'),
    path('product/<int:order_id>/',views.orderProduct,name='orderProduct'),

    path('address/',views.OrderAddress,name='OrderAddress'),
    path('address/<int:address_id>/',views.OrderAddress,name='OrderAddress'),

    path('details/',views.order,name='order'),
    path('details/<int:order_id>/',views.order,name='order'),
]