from django.shortcuts import render
import os
import datetime
from django.shortcuts import render, HttpResponse

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework import status
from rest_framework.permissions import BasePermission, IsAuthenticated, SAFE_METHODS
from django.db.models import Q
from rest_framework import status
from django.contrib.auth.models import User
from .models import OrderedProduct,OrderedAddress,Orders
from .serializer import OrderedProductSerializer,OrderedProductGetSerializer,OrderedAddressGetSerializer,OrderedAddressSerializer,OrdersSerializer,OrdersGetSerializer
@api_view(['POST','GET','PATCH','DELETE'])
@permission_classes([IsAuthenticated])
def orderProduct(request,order_id=0):
    if request.method =='POST':
        try:
            serializer = OrderedProductSerializer(data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response({'Success':'Product Orderd','App_data':'Product ordered.'},status=status.HTTP_201_CREATED)
        except:
            return Response({'Error':'Ordered product Error','app_data':'Error while ordering product'},status=status.HTTP_400_BAD_REQUEST)
    if request.method =='GET':
        try:
            orderdProduct =OrderedProduct.objects.filter(active_status=True)
            serializer = OrderedProductGetSerializer(orderdProduct,many=True)
            return Response(serializer.data)
        except:
            return Response({'Error':'No orders exists','app_data':'No orders found'},status=status.HTTP_400_BAD_REQUEST)    
    if request.method=='PATCH':
        try:
            order = OrderedProduct.objects.get(id=order_id)
            serializer = OrderedProductSerializer(order,data=request.data,partial=True)
            if serializer.is_valid():
                serializer.save()
            return Response(serializer.data)
        except:
            return Response({'Error':'PATCH error','app_data':'Error While updating order'},status=status.HTTP_400_BAD_REQUEST)    
    if request.method=='DELETE':
        try:
            order = OrderedProduct.objects.get(id=order_id)
            order.active_status=False
            order.save()
            return Response({'success':'Order deleted','app_data':'Ordered product deleted'})
        except:
            return Response ({'Error':'Error While delete','app_data':'Error while deleting'},status=status.HTTP_400_BAD_REQUEST    )    
@api_view(['POST','GET','PATCH','DELETE'])
@permission_classes([IsAuthenticated])
def OrderAddress(request,address_id=0):
    if(request.method=='POST'):
        try:
            serializer =OrderedAddressSerializer(data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response({'success':'OrderedAddress is created','app_data':'Address is registered'},status=status.HTTP_201_CREATED)
        except:
            return Response({'Error':'Error in adding address','app_data':'Error in adding address'},status=status.HTTP_400_BAD_REQUEST)       

    if (request.method =='GET'):
        try:
            address=OrderedAddress.objects.filter(active_status=True)
            serializer = OrderedAddressGetSerializer(address,many=True)
            return Response(serializer.data)
        except:
            return Response({'Error':'No orders exists','app_data':'No orders found'},status=status.HTTP_400_BAD_REQUEST)    
    if request.method=='PATCH':
        try:
            address = OrderedAddress.objects.get(id=address_id)
            serializer = OrderedAddressSerializer(address,data=request.data,partial=True)
            if serializer.is_valid():
                serializer.save()
            return Response(serializer.data)
        except:
            return Response({'Error':'PATCH error','app_data':'Error While updating Address'},status=status.HTTP_400_BAD_REQUEST)            

    if request.method=='DELETE':
        try:
            address = OrderedAddress.objects.get(id=address_id)
            address.active_status=False
            address.save()
            return Response({'success':'Order deleted','app_data':'Ordered product deleted'})
        except:
            return Response ({'Error':'Error While delete','app_data':'Error while deleting address'},status=status.HTTP_400_BAD_REQUEST    )            





@api_view(['POST','GET','PATCH','DELETE'])
@permission_classes([IsAuthenticated])
def order(request,order_id=0):
    if(request.method=='POST'):
        try:
            serializer =OrdersSerializer(data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response({'success':'Order is registered','app_data':'order is registered'},status=status.HTTP_201_CREATED)
        except:
            return Response({'Error':'Error in adding order','app_data':'Error in adding order'},status=status.HTTP_400_BAD_REQUEST)       

    if (request.method =='GET'):
        try:
            order=Orders.objects.filter(active_status=True)
            serializer = OrdersGetSerializer(order,many=True)
            return Response(serializer.data)
        except:
            return Response({'Error':'No orders exists','app_data':'No orders found'},status=status.HTTP_400_BAD_REQUEST)    
    if request.method=='PATCH':
        try:
            order = Orders.objects.get(id=order_id)
            serializer = OrdersSerializer(order,data=request.data,partial=True)
            if serializer.is_valid():
                serializer.save()
            return Response(serializer.data)
        except:
            return Response({'Error':'PATCH error','app_data':'Error While updating Address'},status=status.HTTP_400_BAD_REQUEST)            

    if request.method=='DELETE':
        try:
            order = Orders.objects.get(id=order_id)
            order.active_status=False
            order.save()
            return Response({'success':'Order deleted','app_data':'Ordered product deleted'})
        except:
            return Response ({'Error':'Error While delete','app_data':'Error while deleting address'},status=status.HTTP_400_BAD_REQUEST    )            
