from django.db import models

# Create your models here.

class Coupon(models.Model):
    active_status = models.BooleanField(default=True)
    code = models.CharField(max_length=200)
    discount_type_list= (
         ("price", "price"),
         ("percentage","percentage") 
        )
    discount_type = models.CharField( 
    max_length = 50, 
    choices = discount_type_list, 
    default = "price"
    )
    value = models.CharField(max_length=100)