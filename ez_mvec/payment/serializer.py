from rest_framework import serializers
from .models import Coupon

class CouponSerializer(serializers.ModelSerializer):
    class Meta:
        model = Coupon
        fields = ['id','code','discount_type','value']

class CouponGetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Coupon
        fields = '__all__'
