from django.shortcuts import render
import os
import datetime
from django.shortcuts import render, HttpResponse

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework import status
from rest_framework.permissions import BasePermission, IsAuthenticated, SAFE_METHODS
from django.db.models import Q
from rest_framework import status
from django.contrib.auth.models import User
from .models import Coupon
from .serializer import CouponSerializer,CouponGetSerializer


@api_view(['POST','GET','PATCH','DELETE'])
@permission_classes([IsAuthenticated])
def coupon(request,coupon_id=0):
    if request.method =='POST':
        try:
            serializer = CouponSerializer(data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response({'Success':'Coupon added','App_data':'Coupon added.'},status=status.HTTP_201_CREATED)
        except:
            return Response({'Error':'Error in Coupon','app_data':'Error while adding Coupon'},status=status.HTTP_400_BAD_REQUEST)
    if request.method =='GET':
        try:
            sub =Coupon.objects.filter(active_status=True)
            serializer = CouponGetSerializer(sub,many=True)
            return Response(serializer.data)
        except:
            return Response({'Error':'No Coupon exists','app_data':'No Coupon found'},status=status.HTTP_400_BAD_REQUEST)    
    if request.method=='PATCH':
        try:
            cp = Coupon.objects.get(id=coupon_id)
            serializer = CouponSerializer(cp,data=request.data,partial=True)
            if serializer.is_valid():
                serializer.save()
            return Response(serializer.data)
        except:
            return Response({'Error':'PATCH error','app_data':'Error While updating Coupon'},status=status.HTTP_400_BAD_REQUEST)    
    if request.method=='DELETE':
        try:
            cp = Coupon.objects.get(id=coupon_id)
            cp.active_status=False
            cp.save()
            return Response({'success':'Coupon deleted','app_data':'Coupon deleted'})
        except:
            return Response ({'Error':'Error While delete','app_data':'Error while deleting'},status=status.HTTP_400_BAD_REQUEST    )    

    