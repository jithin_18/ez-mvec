from django.contrib import admin
from .models import RecentlyViewed,SubCategory,Product,VariableProduct
# Register your models here.
admin.site.register(RecentlyViewed)
admin.site.register(Product)
admin.site.register(SubCategory)
admin.site.register(VariableProduct)