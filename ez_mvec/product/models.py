from django.db import models
from vendor.models import Vendor
# Create your models here.
class SubCategory(models.Model):
    active_status = models.BooleanField(default=True)
    name = models.CharField(max_length=100)
    category = models.CharField(max_length=100)
class Product(models.Model):
    active_status = models.BooleanField(default=True)
    name = models.CharField(max_length=150)
    sku = models.CharField(max_length=200)
    category = models.CharField(max_length=200,null=True)
    sub_category=models.ManyToManyField(SubCategory,related_name='Sub_category', blank=True) 
    price =models.CharField(max_length=100)
    sale_price = models.CharField(max_length=100)
    image = models.ImageField(upload_to='products/', null=True, blank=True)
    vendor = models.ForeignKey(Vendor,on_delete=models.CASCADE)
class RecentlyViewed(models.Model):
     active_status = models.BooleanField(default=True)
     top_product = models.ForeignKey(Product,on_delete=models.CASCADE)
    #  sub_category = models.ForeignKey()
    #  category = models.CharField(max_length=200,blank=True,null=True)
class VariableProduct(models.Model):
     active_status = models.BooleanField(default=True)
     v_productId = models.IntegerField(null=True)
     product = models.ManyToManyField(Product, related_name='Product')
     image = models.ImageField(upload_to='products/', null=True, blank=True)
     sale_price = models.CharField(max_length=100,null=True)
     price =models.CharField(max_length=100,null=True)

     
    