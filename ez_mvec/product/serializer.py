from rest_framework import serializers
from .models import Product,SubCategory,RecentlyViewed,VariableProduct
from accounts.models import Users
from offers.models import DealoftheDay

class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ['id','name','sku','category','sub_category','price','sale_price','vendor','image']

class ProductGetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = '__all__'



class SubCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = SubCategory
        fields = ['id','name','category']

class SubCategoryGetSerializer(serializers.ModelSerializer):
    class Meta:
        model = SubCategory
        fields = '__all__'   

class RecentlyViewedSerializer(serializers.ModelSerializer):
    class Meta:
        model = RecentlyViewed
        fields = ['id','top_product']

class RecentlyViewedGetSerializer(serializers.ModelSerializer):
    name = serializers.ReadOnlyField(source='top_product.name')
    category = serializers.ReadOnlyField(source='top_product.category')
    price = serializers.ReadOnlyField(source='top_product.price')
    sale_price = serializers.ReadOnlyField(source='top_product.sale_price')
    image = serializers.ImageField(source='top_product.image')
    class Meta:
        model = RecentlyViewed
        fields = '__all__'           

class LocationGetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Users
        fields = ['id','longitude','latitude','place']   



class TodayDealGetSerializer(serializers.ModelSerializer):
    class Meta:
        model = DealoftheDay
        fields = '__all__'    



class ProductSearchGetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = '__all__'  


class VariableProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = VariableProduct
        fields = ['id','v_productId','image','product','price','sale_price']   



class VariableProductGetSerializer(serializers.ModelSerializer):
    class Meta:
        model = VariableProduct
        fields = '__all__'     
