from django.urls import path
from . import views

urlpatterns  =[
    path('sub-category/',views.subCategory,name='subCategory'),
    path('sub-category/<int:sub_id>/',views.subCategory,name='subCategory'),

    path('products/',views.product,name='product'),
    path('products/<int:product_id>/',views.product,name='product'),

    path('recently/',views.recentlyViewed,name='recentlyViewed'),
    path('recently/<int:recent_id>/',views.recentlyViewed,name='recentlyViewed'),



    path('home/<str:user>/',views.home,name='home'),
    path('search/',views.searchProduct,name='searchProduct'),


    path('variable/<int:vr_id>/',views.variableProduct,name='variableProduct')





]