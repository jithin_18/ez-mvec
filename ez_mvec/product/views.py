from django.shortcuts import render
import os
import datetime
from django.shortcuts import render, HttpResponse
from django.utils.timezone import localdate
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework import status
from rest_framework.permissions import BasePermission, IsAuthenticated, SAFE_METHODS
from django.db.models import Q
from rest_framework import status
from django.contrib.auth.models import User
from .models import SubCategory,Product,RecentlyViewed,VariableProduct
from offers.models import DealoftheDay
from accounts.models import Users
from .serializer import (SubCategoryGetSerializer,SubCategorySerializer,
ProductGetSerializer,ProductSerializer,RecentlyViewedGetSerializer,ProductSearchGetSerializer,
LocationGetSerializer,TodayDealGetSerializer,VariableProductSerializer,VariableProductGetSerializer)


@api_view(['POST','GET','PATCH','DELETE'])
@permission_classes([IsAuthenticated])
def subCategory(request,sub_id=0):
    if request.method =='POST':
        try:
            serializer = SubCategorySerializer(data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response({'Success':'sub category added','App_data':'sub category added.'},status=status.HTTP_201_CREATED)
        except:
            return Response({'Error':'Error in sub category','app_data':'Error while adding sub category'},status=status.HTTP_400_BAD_REQUEST)
    if request.method =='GET':
        try:
            sub =SubCategory.objects.filter(active_status=True)
            serializer = SubCategoryGetSerializer(sub,many=True)
            return Response(serializer.data)
        except:
            return Response({'Error':'No Sub_category exists','app_data':'No Sub_category found'},status=status.HTTP_400_BAD_REQUEST)    
    if request.method=='PATCH':
        try:
            sub = SubCategory.objects.get(id=sub_id)
            serializer = SubCategorySerializer(sub,data=request.data,partial=True)
            if serializer.is_valid():
                serializer.save()
            return Response(serializer.data)
        except:
            return Response({'Error':'PATCH error','app_data':'Error While updating sub Category'},status=status.HTTP_400_BAD_REQUEST)    
    if request.method=='DELETE':
        try:
            sub = SubCategory.objects.get(id=sub_id)
            sub.active_status=False
            sub.save()
            return Response({'success':'sub_category deleted','app_data':'sub Category deleted'})
        except:
            return Response ({'Error':'Error While delete','app_data':'Error while deleting'},status=status.HTTP_400_BAD_REQUEST    )    

    
@api_view(['POST','GET','PATCH','DELETE'])
@permission_classes([IsAuthenticated])
def product(request,product_id=0):
    if request.method =='POST':
        try:
            serializer = ProductSerializer(data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response({'Success':'product added','App_data':'product added.'},status=status.HTTP_201_CREATED)
        except:
            return Response({'Error':'Error in product','app_data':'Error In adding product'},status=status.HTTP_400_BAD_REQUEST)
    if request.method =='GET':
        try:
            pr =Product.objects.filter(active_status=True)
            serializer = ProductGetSerializer(pr,many=True)
            return Response(serializer.data)
        except:
            return Response({'Error':'No product exist','app_data':'No product found'},status=status.HTTP_400_BAD_REQUEST)    
    if request.method=='PATCH':
        try:
            pr = Product.objects.get(id=product_id)
            serializer = ProductSerializer(pr,data=request.data,partial=True)
            if serializer.is_valid():
                serializer.save()
            return Response(serializer.data)
        except:
            return Response({'Error':'PATCH error','app_data':'Error While updating product'},status=status.HTTP_400_BAD_REQUEST)    
    if request.method=='DELETE':
        try:
            pr = Product.objects.get(id=product_id)
            pr.active_status=False
            pr.save()
            return Response({'success':'product deleted','app_data':'product deleted'})
        except:
            return Response ({'Error':'Error While delete','app_data':'Error while deleting'},status=status.HTTP_400_BAD_REQUEST    ) 


@api_view(['POST','GET','PATCH','DELETE'])
@permission_classes([IsAuthenticated])
def recentlyViewed(request,recent_id=0):
    if request.method =='POST':
        try:
            serializer = RecentlyViewedSerializer(data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response({'Success':'Recent product added','App_data':'Recent product added.'},status=status.HTTP_201_CREATED)
        except:
            return Response({'Error':'Error in recent product','app_data':'Error In adding recent product'},status=status.HTTP_400_BAD_REQUEST)
    if request.method =='GET':
        try:
            rc =RecentlyViewed.objects.filter(active_status=True)
            serializer = RecentlyViewedGetSerializer(rc,many=True)
            return Response(serializer.data)
        except:
            return Response({'Error':'No recent product exist','app_data':'No recent product found'},status=status.HTTP_400_BAD_REQUEST)    
    if request.method=='PATCH':
        try:
            rc = RecentlyViewed.objects.get(id=recent_id)
            serializer = RecentlyViewedSerializer(rc,data=request.data,partial=True)
            if serializer.is_valid():
                serializer.save()
            return Response(serializer.data)
        except:
            return Response({'Error':'PATCH error','app_data':'Error While updating product'},status=status.HTTP_400_BAD_REQUEST)    
    if request.method=='DELETE':
        try:
            pr = RecentlyViewed.objects.get(id=recent_id)
            pr.active_status=False
            pr.save()
            return Response({'success':'product deleted','app_data':'product deleted'})
        except:
            return Response ({'Error':'Error While delete','app_data':'Error while deleting'},status=status.HTTP_400_BAD_REQUEST    ) 


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def home(request,user):

    if request.method =='GET':
        app_data = {}
        use_obj = Users.objects.get(username = user)   
        serializer = LocationGetSerializer(use_obj)
        app_data['location'] =serializer.data
        deal_obj =DealoftheDay.objects.filter(active_status=True).filter(date=datetime.datetime.now())
        deal_serializer = TodayDealGetSerializer(deal_obj,many=True)
        app_data['deal']=deal_serializer.data
        recent_obj =RecentlyViewed.objects.filter(active_status=True)
        recent_serializer = RecentlyViewedGetSerializer(recent_obj,many=True)
        app_data['Recently_viewed'] =recent_serializer.data

        popular = Product.objects.filter(active_status=True)
        pr_serializer = ProductGetSerializer(popular,many=True)
        app_data['popular_products'] = pr_serializer.data
        return Response ({'app_data':app_data})



@api_view(['POST'])
@permission_classes([IsAuthenticated])
def searchProduct(request):
    if request.method == 'POST':
        try:
            key = request.POST['key']
            report_found=Product.objects.filter(Q(sub_category__name__istartswith=key) | 
            Q(name__istartswith=key) | Q(category__istartswith=key))
            serializer = ProductSearchGetSerializer(report_found, many=True)
            print(serializer.data)
            return Response(serializer.data, status=status.HTTP_200_OK)
        except:
            return Response({"Fields Required": "key, product"}, status=status.HTTP_400_BAD_REQUEST)
                    

@api_view(['POST','GET','PATCH','DELETE'])
@permission_classes([IsAuthenticated])
def variableProduct(request,vr_id=0):
    if request.method =='POST':
        try:
            serializer = VariableProductSerializer(data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response({'Success':'Variable product added','App_data':'Variable product added.'},status=status.HTTP_201_CREATED)
        except:
            return Response({'Error':'Error in Variable product','app_data':'Error while adding Variable product'},status=status.HTTP_400_BAD_REQUEST)
    if request.method =='GET':
        try:
            vr =VariableProduct.objects.filter(active_status=True)
            serializer = VariableProductGetSerializer(vr,many=True)
            return Response(serializer.data)
        except:
            return Response({'Error':'No Varience exists','app_data':'No Varience found'},status=status.HTTP_400_BAD_REQUEST)    
    if request.method=='PATCH':
        try:
            vr = VariableProduct.objects.get(id=vr_id)
            serializer = VariableProductSerializer(vr,data=request.data,partial=True)
            if serializer.is_valid():
                serializer.save()
            return Response(serializer.data)
        except:
            return Response({'Error':'PATCH error','app_data':'Error While updating varience'},status=status.HTTP_400_BAD_REQUEST)    
    if request.method=='DELETE':
        try:
            vr = VariableProduct.objects.get(id=vr_id)
            vr.active_status=False
            vr.save()
            return Response({'success':'varience deleted','app_data':'varience deleted'})
        except:
            return Response ({'Error':'Error While delete','app_data':'Error while deleting'},status=status.HTTP_400_BAD_REQUEST    )    
