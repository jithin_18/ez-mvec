from django.db import models
from django.contrib.auth.models import User

class Vendor(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True)
    active_status = models.BooleanField(default=True,null=True,blank=True)
    username = models.EmailField(null=True,blank=True)
    password =models.CharField(max_length=200,null=True,blank=True)
    state = models.CharField(max_length=200,null=True,blank=True)
    district = models.CharField(max_length=200,null=True,blank=True)
    thaluk = models.CharField(max_length=200,null=True,blank=True)
    pin_code = models.CharField(max_length=200,null=True,blank=True)
    bank_name = models.CharField(max_length=200,null=True,blank=True)
    branch = models.CharField(max_length=200,null=True,blank=True)
    state = models.CharField(max_length=200,null=True,blank=True)
    account_name = models.CharField(max_length=200,null=True,blank=True)
    account_types= (
         ("savings", "savings"),
         ("current","current"),
        ("fixed depsit","fixed depsit"),
        ("salary account","salary account"),
        ("recurring deposit account","recurring deposit account")
        )
    account_type = models.CharField( 
    max_length = 50, 
    choices = account_types, 
    default = "savings"
    )
    vendor_type_list= (
         ("local", "local"),
         ("common","common") 
        )
    vendor_type = models.CharField( 
    max_length = 50, 
    choices = vendor_type_list, 
    default = "common"
    )
    ifsc =models.CharField(max_length=200,null=True)
    full_name =models.CharField(max_length=200,null=True)
    shop_name = models.CharField(max_length=200,null=True)
    shop_address = models.CharField(max_length=200,null=True)
    phone_number = models.CharField(max_length=200,null=True)
    whatsup_number = models.CharField(max_length=200,null=True)
    id_proof = models.CharField(max_length=200,null=True)
    id_proof_upload = models.FileField(max_length=200,null=True)
    gstin_number = models.CharField(max_length=200,null=True)
    registration_fee = models.CharField(max_length=200,null=True)
    payment_gateway_ref_no = models.CharField(max_length=200,null=True)
    name = models.CharField(max_length=200,null=True)
    code = models.CharField(max_length=100,null=True)
    address = models.CharField(max_length=250,null=True)