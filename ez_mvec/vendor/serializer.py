from rest_framework import serializers
from .models import Vendor

class VendorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vendor
        fields = ['id','username','password','state','district',
        'thaluk','pin_code','bank_name','branch','state','account_type',
        'phone_number','account_name','vendor_type','ifsc','full_name',
        'shop_name','shop_address','id_proof','whatsup_number',
        'id_proof_upload','gstin_number','registration_fee','payment_gateway_ref_no','name','code','address']

class VendorGetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vendor
        fields = '__all__'