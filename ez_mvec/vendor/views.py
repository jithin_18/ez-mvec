from django.shortcuts import render
import os
import datetime
from django.shortcuts import render, HttpResponse

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework import status
from rest_framework.permissions import BasePermission, IsAuthenticated, SAFE_METHODS
from django.db.models import Q
from rest_framework import status
from django.contrib.auth.models import User
from django.contrib.auth import login,authenticate
from .models import Vendor
from .serializer import VendorGetSerializer,VendorSerializer

@api_view(['POST','GET','PATCH','DELETE'])
@permission_classes([IsAuthenticated])
def vendor(request,vendor_id=0):
    if request.method =='POST':
        username = request.POST['username']
        password = request.POST['password']


        serializer = VendorSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
        else:  
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)   
        try:
            if(User.objects.filter(username=username).exists()):
                return Response({'Error': 'Username is already exist','app_data': 'Invalid credentials'}, status=status.HTTP_400_BAD_REQUEST)                         
            user = User.objects.create_user(username=username,email=username,password=password)
            user.save()
            vendor_temp = Vendor.objects.get(id=serializer.data['id'])
            vendor_temp.user=user
            vendor_temp.save()
            return Response({'Success': 'Vendor created','app_data': 'Vendor Created '}, status.HTTP_201_CREATED)     
        except:
            return Response({'Error':'Error in vendor','app_data':'Error while adding Vendor'},status=status.HTTP_400_BAD_REQUEST)
    if request.method =='GET':
        try:
            ven =Vendor.objects.filter(active_status=True)
            serializer = VendorGetSerializer(ven,many=True)
            return Response(serializer.data)
        except:
            return Response({'Error':'No vendor exists','app_data':'No vendor found'},status=status.HTTP_400_BAD_REQUEST)    
    if request.method=='PATCH':
        try:
            ven = Vendor.objects.get(id=vendor_id)
            serializer = VendorSerializer(ven,data=request.data,partial=True)
            if serializer.is_valid():
                serializer.save()
            return Response(serializer.data)
        except:
            return Response({'Error':'PATCH error','app_data':'Error While updating vendor'},status=status.HTTP_400_BAD_REQUEST)    
    if request.method=='DELETE':
        try:
            ven = Vendor.objects.get(id=vendor_id)
            ven.active_status=False
            ven.save()
            return Response({'success':'vendor deleted','app_data':'vendor deleted'})
        except:
            return Response ({'Error':'Error While delete','app_data':'Error while deleting'},status=status.HTTP_400_BAD_REQUEST    )    


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def vendorLogin(request):
    if request.method =='POST':
        print('haii')
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username,password=password)
        print(user)
        if user is not None:
            if user.is_active:
               return Response ({'success':'Valid user','app_data':'valid user'})
            else:
                print("diabled")# Return a 'disabled account' error message
        else:
            # Return an 'invalid login' error message.
            print("invalid")